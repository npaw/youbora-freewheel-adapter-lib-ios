//
//  YouboraFreewheelAdapter.h
//  YouboraFreewheelAdapter
//
//  Created by Enrique Alfonso Burillo on 25/01/2018.
//  Copyright © 2018 Enrique Alfonso. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for YouboraFreewheelAdapter.
FOUNDATION_EXPORT double YouboraFreewheelAdapterVersionNumber;

//! Project version string for YouboraFreewheelAdapter.
FOUNDATION_EXPORT const unsigned char YouboraFreewheelAdapterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YouboraFreewheelAdapter/PublicHeader.h>
#import <YouboraFreewheelAdapter/YBFreewheelAdapter.h>

